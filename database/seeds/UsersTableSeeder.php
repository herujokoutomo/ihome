<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Heru Joko',
            'email' => 'herujokoutomo@gmail.com',
            'password' => '12345'
        ]);

        User::create([
            'name' => 'Partner 1',
            'email' => 'partner1@gmail.com',
            'password' => '12345',
            'role' => 'partner'
        ]);

        User::create([
            'name' => 'Partner 2',
            'email' => 'partner2@gmail.com',
            'password' => '12345',
            'role' => 'partner'
        ]);

        User::create([
            'name' => 'Partner 3',
            'email' => 'partner3@gmail.com',
            'password' => '12345',
            'role' => 'partner'
        ]);
    }
}
