<?php

use Illuminate\Database\Seeder;

class JobsAndGroups extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $group = \App\JobGroup::create([
            'name' => 'Service AC'
        ]);
        \App\Job::create([
            'name' => 'Isi Ulang Freon',
            'job_groups_id' => $group->id
        ]);
        \App\Job::create([
            'name' => 'Pembersihan Filter',
            'job_groups_id' => $group->id
        ]);

        \App\PartnerJob::create([
            'partner_id' => 2,
            'job_id' => 2
        ]);

        \App\PartnerJob::create([
            'partner_id' => 2,
            'job_id' => 3
        ]);

        \App\PartnerJob::create([
            'partner_id' => 3,
            'job_id' => 2
        ]);
    }
}
