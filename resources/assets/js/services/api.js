import axios from 'axios';
import localstorage from 'localstorage';

const BASE_URL = '/api/v1';

const writeLog = function (data) {
    return new Promise((resolve, reject) => {
        axios.post(BASE_URL + '/log', data).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    });
};

const signin = function (data) {
    return new Promise((resolve, reject) => {
        axios.post(BASE_URL + '/signin', data).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    });
};

const fetchJobs = function () {
    let token = localStorage.getItem('token');
    return new Promise((resolve, reject) => {
        axios.get(BASE_URL + '/jobs', {
            headers: {
                "Authorization": token
            }
        }).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    });
};

const fetchCategories = function () {
    let token = localStorage.getItem('token');
    return new Promise((resolve, reject) => {
        axios.get(BASE_URL + '/categories', {
            headers: {
                "Authorization": token
            }
        }).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    });
};

const createJob = function (data) {
    let token = localStorage.getItem('token');
    return new Promise((resolve, reject) => {
        axios.post(BASE_URL + '/jobs',data ,{
            headers: {
                "Authorization": token
            }
        }).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    });
};

const viewJob = function (id) {
    let token = localStorage.getItem('token');
    return new Promise((resolve, reject) => {
        axios.get(BASE_URL + '/jobs/'+id ,{
            headers: {
                "Authorization": token
            }
        }).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    });
};

const deleteJob = function (id) {
    let token = localStorage.getItem('token');
    return new Promise((resolve, reject) => {
        axios.delete(BASE_URL + '/jobs/'+id ,{
            headers: {
                "Authorization": token
            }
        }).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    });
};

const updateJob = function (id, data) {
    let token = localStorage.getItem('token');
    return new Promise((resolve, reject) => {
        axios.put(BASE_URL + '/jobs/'+id, data,{
            headers: {
                "Authorization": token
            }
        }).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    });
};

const fetchDiscounts = function () {
    let token = localStorage.getItem('token');
    return new Promise((resolve, reject) => {
        axios.get(BASE_URL + '/discounts', {
            headers: {
                "Authorization": token
            }
        }).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    });
};

const createDiscount = function (data) {
    let token = localStorage.getItem('token');
    return new Promise((resolve, reject) => {
        axios.post(BASE_URL + '/discounts', data,{
            headers: {
                "Authorization": token
            }
        }).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    });
};

const deleteDiscount = function (id) {
    let token = localStorage.getItem('token');
    return new Promise((resolve, reject) => {
        axios.delete(BASE_URL + '/discounts/' + id,{
            headers: {
                "Authorization": token
            }
        }).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    });
};


export default {
    writeLog,
    signin,
    fetchJobs,
    fetchCategories,
    createJob,
    viewJob,
    deleteJob,
    updateJob,
    fetchDiscounts,
    createDiscount,
    deleteDiscount
}