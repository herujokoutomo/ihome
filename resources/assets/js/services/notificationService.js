import toastr from 'toastr';
import api from './api';

function notifyError(err, context = 'Oops!') {
    // if error do error logging
    api.writeLog({
        type: 'ERROR',
        text: err.response.data
    });

    toastr.error(err.response.data, context);
}

function notifySuccess(message, context = 'Success!') {
    toastr.success(message, context)
}

export default {
    notifyError,
    notifySuccess
}