import numeral from 'numeral';
import $ from 'jquery';

const NUM_FORMAT = '0,0';

export default {
    inserted: function (el) {
        let text = $(el).text();
        let number = Number(text);
        $(el).text(numeral(number).format(NUM_FORMAT));
    },
    update: function (el) {
        console.log('updated');
        let text = $(el).text();
        if(typeof(text) !== 'string'){
            let number = Number(text);
            $(el).text(numeral(number).format(NUM_FORMAT));
        }
    }
}