import dataTable from 'datatables.net';
import $ from 'jquery';

var table = null;
export default {
    inserted: function (el) {
        table = $(el).dataTable();
    },
    update: function (el) {
        $(el).DataTable().destroy();
        setTimeout(() => {
            table = $(el).dataTable();
        },100)


    }
}