
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import priceFormat from './directives/priceFormat';
import datatable from './directives/datatable';
import AwesomeMask from 'awesome-mask'
require('./bootstrap');

window.Vue = require('vue');

import Main from './main';
import VueRouter from 'vue-router';
import VeeValidate from 'vee-validate';

Vue.use(VueRouter);
Vue.use(VeeValidate);

Vue.directive('price', priceFormat);
Vue.directive('datatable', datatable);
Vue.directive('awesomeMask', AwesomeMask);

window.isLogin = false;

// pages
import Dashboard from './pages/dashboard';
import Jobs from './pages/jobs/jobs';
import NewJobs from './pages/jobs/new';
import EditJobs from './pages/jobs/edit';
import Discounts from './pages/discounts/discounts';
import NewDiscounts from './pages/discounts/new';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
console.log('running');

const routes = [
    { path: '/', component: Dashboard },
    { path: '/jobs', component: Jobs },
    { path: '/newjobs', component: NewJobs },
    { path: '/jobs/:id', component: EditJobs },
    { path: '/discounts', component: Discounts },
    { path: '/newdiscounts', component: NewDiscounts },
];

const router = new VueRouter({
    routes // short for `routes: routes`
});

const app = new Vue({
    router,
    components: {
        ihome: Main
    }
}).$mount('#app')
