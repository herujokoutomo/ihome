<html>
    <head>
        <link rel="stylesheet" type="text/css" href="{{ url('css/application.css') }}" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" />
    </head>
    <body>
        <div id="app">
            <ihome></ihome>
        </div>
    </body>
    <script type="text/javascript" src="{{ url('js/app.js') }}"></script>
</html>