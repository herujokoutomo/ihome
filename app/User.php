<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\AsPartner;
use App\Traits\AsCustomer;

class User extends Authenticatable
{
    use Notifiable;
    use AsPartner;
    use AsCustomer;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // attributes

    public function setPasswordAttribute($password){
        $this->attributes['password'] = bcrypt($password);
    }

    // scope
    public function scopeCustomer($query){
        return $query->where('role','customer');
    }

    public function scopePartner($query){
        return $query->where('role','partner');
    }

    public function scopeAdmin($query){
        return $query->where('role','admin');
    }
}
