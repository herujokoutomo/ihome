<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class PartnerJob extends Model
{
    protected $fillable = ['partner_id','job_id'];

    public function partner(){
        return User::find($this->partner_id);
    }
}
