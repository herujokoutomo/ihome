<?php

namespace App\Traits;

use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyNewRegisteredCustomer;

trait AsCustomer {

    public function requestVerification(){
        $this->verification_token = uniqid();

        Mail::to($this->email)
            ->queue(new VerifyNewRegisteredCustomer($this->verification_token));
    }

}