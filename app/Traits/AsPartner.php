<?php

namespace App\Traits;

use App\PartnerJob;
use App\Job;

trait AsPartner {

    public function addJob($jobs){
        if(gettype($jobs) === 'string'){
            $jobs = json_decode($jobs);
        }
        foreach ($jobs as $job){
            PartnerJob::create([
                'partner_id' => $this->id,
                'job_id' => $job
            ]);
        }
    }

    public function updateJob($jobs){
        $this->deleteJob();
        $this->addJob($jobs);
    }

    public function deleteJob(){
        $relations = PartnerJob::where('partner_id',$this->id)->get();
        foreach ($relations as $r){
            $r->delete();
        }
    }

    public function jobs(){
        $job_ids = PartnerJob::where('partner_id',$this->id)->get();
        $jobs = collect();
        foreach($job_ids as $j){
            $job = Job::find($j->job_id);
            $jobs->push($job);
        }
        return $jobs;
    }

}