<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Job;

class Discount extends Model
{
    protected $fillable = ["percentage", "start", "end", "description", "job_id", "minimum_qty"];

    protected $casts = [
        'percentage' => 'float',
        'minimum_qty' => 'integer'
    ];

    public function setStartAttribute($start){
        $this->attributes['start'] = Carbon::parse($start);
    }

    public function setEndAttribute($end){
        $this->attributes['end'] = Carbon::parse($end);
    }

    public function job(){
        return Job::find($this->job_id);
    }
}
