<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Discount;
use Carbon\Carbon;

class Job extends Model
{
    protected $fillable = ['name', 'avatar', 'description', 'job_groups_id', 'price'];

    public function partners(){
        $rel = PartnerJob::where('job_id',$this->id)->get();
        $partners = collect();
        foreach($rel as $r){
            $partners->push($r->partner());
        }
        return $rel;
    }

    public function activeDiscount(){
        $discount = Discount::whereDate('start', '<=', Carbon::now())
            ->where('job_id', $this->id)
            ->whereDate('start', '<=', Carbon::now())
            ->orderBy('start', 'asc')
            ->get()->last();

        if(isset($discount->end)){
            $end = Carbon::parse($discount->end);
            if($end->gt(Carbon::now())){
                return $end;
            } else {
                return null;
            }
        } else {
            return $discount;
        }
    }
}
