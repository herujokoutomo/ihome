<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobGroup extends Model
{
    protected $fillable = ['name','avatar','description'];

    public function jobs(){
        return $this->hasMany('App\Job','job_groups_id','id');
    }
}
