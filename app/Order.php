<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public static function boot()
    {
        static::creating(function ($model) {
            $model->order_id = uniqid();
        });

        parent::boot();
    }

    protected $fillable = ['customer_id', 'partner_id', 'job_id', 'status', 'price', 'address', 'notes', 'address_lat', 'address_lng'];

    public function customer()
    {
        return $this->belongsTo('App\User', 'customer_id');
    }

    public function partner()
    {
        return $this->belongsTo('App\User', 'partner_id');
    }

    public function job()
    {
        return $this->belongsTo('App\Job', 'job_id');
    }

    // functions

    public function updateStatus($status,$notes = "")
    {
        $this->status = $status;
        $this->save();
        $this->recordHistory($notes);
    }

    public function recordHistory($notes){
        OrderHistory::create([
            'order_id' => $this->order_id,
            'status' => $this->status,
            'notes' => $notes
        ]);
    }
}
