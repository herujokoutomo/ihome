<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\JsonWebToken;
use App\User;

class JWTAdminOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        if($token){
            try {
                $decoded = JsonWebToken::decode($token);
                $request->current_user = User::find($decoded->id);

                if($request->current_user->role != 'admin'){
                    return response()->json('user must be admin', 403);
                } else {
                    return $next($request);
                }

            } catch (\Exception $e){
                return response()->json('invalid token', 403);
            }
        } else {
            return response()->json('token required', 403);
        }
    }
}
