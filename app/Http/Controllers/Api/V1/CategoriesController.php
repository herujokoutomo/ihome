<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\JobGroup;

class CategoriesController extends Controller
{
    public function index(){
        $categories = JobGroup::all();
        return response()->json($categories);
    }
}
