<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Image;

class AvatarController extends Controller
{
    public function customerAvatar(Request $request){
        $validator = Validator::make($request->all(), [
            'avatar' => 'required|file|mimes:jpeg,png|max:2000'
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $basePath = 'public/avatars/'.$request->current_user->id;

        $path = $this->storeAvatar($request, $basePath);
        $request->current_user->avatar = $path;
        $request->current_user->save();

        return response()->json($path);
    }

    public function partnerAvatar(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'avatar' => 'required|file|mimes:jpeg,png|max:2000'
        ]);

        if($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $partner = User::find($id);

        $basePath = 'public/avatars/'.$partner->id;

        $path = $this->storeAvatar($request, $basePath);
        $partner->avatar = $path;
        $partner->save();

        return response()->json($path);
    }


    private function storeAvatar(Request $request, $basePath = "images") {
        $filename = uniqid();
        $fileExtension = $request->avatar->getClientOriginalExtension();

        $path = $request->avatar->storeAs($basePath, $filename.'.'.$fileExtension);
        $paths = explode('/', $path);
        $paths[0] = url('/storage');
        return join('/', $paths);
    }
}
