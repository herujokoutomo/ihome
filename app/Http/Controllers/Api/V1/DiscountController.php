<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Discount;
use Validator;
use App\ValidatorResult;

class DiscountController extends Controller
{
    public function index(){
        $discounts = Discount::all();
        $discounts->map(function ($d) {
            $d['job'] = $d->job();
        });
        return response()->json($discounts);
    }

    public function show($id){
        $discount = Discount::find($id);

        if($discount){
            return response()->json($discount);
        } else {
            return response()->json('not found', 404);
        }
    }

    public function store(Request $request){
        $validation = $this->discountValid($request);

        if($validation->success()){
            $discount = Discount::create($request->all());

            if($request->autogen == "true"){
                $discount->code = uniqid();
                $discount->save();
            } else{
                if(!$request->code){
                    return response()->json('code is required', 400);
                }
                $discount->code = $request->code;
                $discount->save();
            }

            return response()->json($discount);
        } else {
            return response()->json($validation->errors, 400);
        }
    }

    public function update(Request $request, $id){
        $validation = $this->discountValid($request);

        if($validation->success()){
            $discount = Discount::find($id);

            if($discount){
                $discount->update($request->all());
                return response()->json($discount);
            } else {
                return response()->json('not found', 404);
            }

        } else {
            return response()->json($validation->errors, 400);
        }
    }

    public function destroy($id) {
        $discount = Discount::find($id);

        if($discount){
            $discount->delete();
            return response()->json('deleted');
        } else {
            return response()->json('not found', 404);
        }
    }

    private function discountValid(Request $request){
        $validator = Validator::make($request->all(), [
            'percentage' => 'required',
            'job_id' => 'required',
            'start' => 'required',
            'autogen' => 'required'
        ]);

        if($validator->fails()){
            return new ValidatorResult('fail',$validator->errors());
        } else {
            return new ValidatorResult('success',null);
        }
    }
}
