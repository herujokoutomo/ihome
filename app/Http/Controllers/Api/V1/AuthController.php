<?php

namespace App\Http\Controllers\Api\V1;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\JsonWebToken;

/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     host="api.host.com",
 *     basePath="/api/v1",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="I-home",
 *         description="Api V1",
 *         @SWG\Contact(
 *             email="herujokoutomo@gmail.com"
 *         )
 *     )
 * )
 */

class AuthController extends Controller
{
    /**
     * @SWG\Post(
     *   path="/api/v1/signin",
     *   summary="signin",
     *   tags={"auth"},
     *  @SWG\Parameter(
     *     in="body",
     *     name="email",
     *     description="email",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     *   ),
     *  @SWG\Parameter(
     *     in="body",
     *     name="password",
     *     description="password",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="User object with token"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   )
     * )
     */

    public function signin(Request $request){
        $user = User::where('email',$request->email)->first();
        if($user){
            if(Auth::attempt(['email' => $request->email,'password' => $request->password])){
                $payload = ['id' => $user->id];
                $token = JsonWebToken::encode($payload);
                $data = [
                    'user' => $user,
                    'token' => $token
                ];
                return response()->json($data);
            } else {
                return response()->json('invalid email or password',401);
            }
        } else {
            return response()->json('not found',404);
        }
    }

    public function signout(){

    }
}