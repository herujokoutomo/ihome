<?php

namespace App\Http\Controllers\api\v1;

use function Couchbase\defaultDecoder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;
use App\Http\Controllers\Controller;

class LogController extends Controller
{
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'text' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json('failed writing log', 400);
        }

        switch ($request->type){
            case 'ACCESS':
                Log::useFiles(storage_path('logs/info.log'),'info');
                Log::info($request->text);
                break;
            case 'ERROR':
                Log::useFiles(storage_path('logs/error.log'),'error');
                Log::error($request->text);
                break;
            default:
                break;
        }

        return response()->json('log saved.');
    }
}
