<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use Validator;
use App\ValidatorResult;
use App\Helpers\NotificationHelper;
use Carbon\Carbon;
use App\Job;

class OrderController extends Controller
{
    public function requestOrder(Request $request){
        $validation = $this->requestOrderValid($request);

        if($validation->success()){
            $job = Job::find($request->job_id);

            $accumulativePrice = $job->price * $request->qty;

            // discounts
            $discount = $job->activeDiscount();
            if($discount != null){
                if($request->qty >= $discount->minimum_qty){
                    $accumulativePrice = $accumulativePrice - (($discount->percentage/100) * $accumulativePrice);
                }
            }

            return response()->json($accumulativePrice);
        } else {
            return response()->json($validation->errors,400);
        }

    }

    public function confirmOrder(Request $request){
        $validation = $this->orderValid($request);

        if($validation->success()){
            $order = Order::create([
                'customer_id' => $request->current_user->id,
                'partner_id' => intval($request->partner_id),
                'job_id' => $request->job_id,
                'status' => 'placed',
                'price' => intval($request->price),
                'address' => $request->address,
                'notes' => $request->notes,
                'address_lat' => $request->address_lat,
                'address_lng' => $request->address_lng,
            ]);

            $message = "[".$order->status."] transaksi ".$order->order_id." sebesar Rp ".number_format($order->price)." di alamat : ".$order->address." tercatat ".Carbon::parse($order->created_at)->setTimezone('Asia/Makassar')->toDateTimeString();

            NotificationHelper::notifyInTelegram($message);

            return response()->json($order);
        } else {
            return response()->json($validation->errors,400);
        }
    }

    public function updateStatus(Request $request, $order_id){
        $validation = $this->orderStatusValid($request);

        if($validation->success()){
            $order = Order::where('order_id', $order_id)->first();

            if($order) {
                $order->status = $request->status;
                $order->save();

                $message = "[".$order->status."] transaksi ".$order->order_id." sebesar Rp ".number_format($order->price)." di alamat : ".$order->address." tercatat ".Carbon::parse($order->created_at)->setTimezone('Asia/Makassar')->toDateTimeString();

                NotificationHelper::notifyInTelegram($message);

                return response()->json($order);
            } else {
                return response()->json('not found', 404);
            }
        } else {
            return response()->json($validation->errors,400);
        }
    }

    private function orderStatusValid(Request $request){
        $validator = Validator::make($request->all(), [
            'status' => 'required'
        ]);

        if($validator->fails()){
            return new ValidatorResult('fail',$validator->errors());
        } else {
            return new ValidatorResult('success',null);
        }
    }

    private function requestOrderValid(Request $request){
        $validator = Validator::make($request->all(), [
            'partner_id' => 'required|numeric',
            'job_id' => 'required|numeric',
            'qty' => 'required|numeric'
        ]);

        if($validator->fails()){
            return new ValidatorResult('fail',$validator->errors());
        } else {
            return new ValidatorResult('success',null);
        }
    }

    private function orderValid(Request $request){
        $validator = Validator::make($request->all(), [
            'partner_id' => 'required|numeric',
            'job_id' => 'required|numeric',
            'price' => 'required|numeric',
            'address' => 'required',
        ]);

        if($validator->fails()){
            return new ValidatorResult('fail',$validator->errors());
        } else {
            return new ValidatorResult('success',null);
        }
    }
}
