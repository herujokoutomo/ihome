<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Telegram;
use App\Helpers\NotificationHelper;

class NotificationController extends Controller
{
    public function telegram(){
        NotificationHelper::notifyInTelegram('helper');
    }
}