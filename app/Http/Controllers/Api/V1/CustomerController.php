<?php

namespace App\Http\Controllers\Api\V1;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use App\ValidatorResult;
use App\Helpers\NotificationHelper;
use Carbon\Carbon;

class CustomerController extends Controller
{
    /**
     * @SWG\Post(
     *   path="/api/v1/register",
     *   summary="register",
     *   tags={"customers"},
     *  @SWG\Parameter(
     *     in="body",
     *     name="name",
     *     description="name",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     *   ),
     *  @SWG\Parameter(
     *     in="body",
     *     name="email",
     *     description="email",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     *   ),
     *  @SWG\Parameter(
     *     in="body",
     *     name="password",
     *     description="password",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     *   ),
     *  @SWG\Parameter(
     *     in="body",
     *     name="role",
     *     description="role",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="User object with token"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   )
     * )
     */

    public function register(Request $request){
        $validation = $this->userValid($request);

        if($validation->success()){
            $customer = User::create($request->all());

            $customer->requestVerification();

            return response()->json($customer);
        } else {
            return response()->json($validation->errors,400);
        }
    }

    public function transactions(Request $request){
        $transactions = Order::where('customer_id', $request->current_user->id)
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json($transactions);
    }

    private function userValid(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
            'role' => 'required'
        ]);

        if($validator->fails() || ($request->role != 'customer')){
            return new ValidatorResult('fail',$validator->errors());
        } else {
            return new ValidatorResult('success',null);
        }
    }
}
