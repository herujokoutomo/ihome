<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use App\ValidatorResult;

class PartnerController extends Controller
{
    public function index(){
        $partners = User::partner()->get();
        foreach($partners as $p){
            $p['jobs'] = $p->jobs();
        }
        return response()->json($partners);
    }

    public function store(Request $request){
        $validation = $this->userValid($request);
        if($validation->success()){
            $partner = User::create($request->all());
            $partner->role = 'partner';
            $partner->save();
            if($request->jobs){
                $partner->addJob($request->jobs);
            }
            return response()->json($partner);
        } else {
            return response()->json($validation->errors,400);
        }
    }

    public function update($id,Request $request){
        $partner = User::partner()->where('id',$id)->first();
        if($partner){
            $partner->update($request->all());
            $partner->updateJob($request->jobs);
            return response()->json($partner);
        } else {
            return response()->json('not found',404);
        }
    }

    public function destroy($id){
        $partner = User::partner()->where('id',$id)->first();
        if($partner){
            $partner->delete();
            return response()->json('partner deleted');
        } else {
            return response()->json('not found',404);
        }
    }

    public function show($id){
        $partner = User::partner()->where('id',$id)->first();
        if($partner){
            return response()->json($partner);
        } else {
            return response()->json('not found',404);
        }
    }

    private function userValid(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return new ValidatorResult('fail',$validator->errors());
        } else {
            return new ValidatorResult('success',null);
        }
    }
}
