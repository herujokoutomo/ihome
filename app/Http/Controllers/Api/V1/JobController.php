<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Job;
use Validator;
use App\ValidatorResult;

class JobController extends Controller
{
    public function index(){
        $jobs = Job::all();
        return response()->json($jobs);
    }

    public function show($id){
        try {
            $jobs = Job::find($id);
            if($jobs){
                return response()->json($jobs);
            } else {
                return response()->json('not found', 404);
            }
        } catch(\Exception $e) {
            return response()->json('not found', 404);
        }

    }

    public function store(Request $request){

        $validation = $this->jobValid($request);

        if($validation->success()){
            $job = Job::create($request->all());
            return response()->json($job);
        } else {
            return response()->json($validation->errors,400);
        }
    }

    public function update(Request $request, $id){

        $validation = $this->jobValid($request);

        if($validation->success()){
            $jobs = Job::find($id);
            if($jobs){
                $jobs->update($request->all());
                return response()->json($jobs);
            } else {
                return response()->json('not found', 404);
            }
        } else {
            return response()->json($validation->errors,400);
        }
    }

    public function destroy($id){
        try {
            $jobs = Job::find($id);
            if($jobs){
                $jobs->delete();
                return response()->json('success');
            } else {
                return response()->json('not found', 404);
            }
        } catch(\Exception $e) {
            return response()->json('not found', 404);
        }

    }

    private function jobValid(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'job_groups_id' => 'required|numeric',
            'price' => 'required|numeric',
        ]);

        if($validator->fails()){
            return new ValidatorResult('fail',$validator->errors());
        } else {
            return new ValidatorResult('success',null);
        }
    }
}
