<?php

namespace App;

use Illuminate\Support\MessageBag;

class ValidatorResult {

    public $status;
    public $errors;

    function __construct($status, MessageBag $errors = null){
        $this->errors = $errors;
        $this->status = $status;
    }

    public function success(){
        return $this->status == 'success';
    }
}