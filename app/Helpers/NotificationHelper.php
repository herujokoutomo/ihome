<?php

namespace App\Helpers;

use Telegram\Bot\Laravel\Facades\Telegram;

class NotificationHelper {

    public static function notifyInTelegram($message){
        $params = [
            'chat_id'                  => -227468994,
            'text'                     => $message
        ];

        Telegram::sendMessage($params);
    }

}