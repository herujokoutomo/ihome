<?php

namespace App\Helpers;

use Firebase\JWT\JWT;

class JsonWebToken
{
    public static function encode($payload){
        return JWT::encode($payload,env('ID_TOKEN_KEY'));
    }

    public static function decode($token){
        return JWT::decode($token,env('ID_TOKEN_KEY'),array('HS256'));
    }
}