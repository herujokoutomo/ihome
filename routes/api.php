<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['namespace' => 'Api'], function () {
    Route::group(['namespace' => 'V1', 'prefix' => 'v1'], function () {
        // Dev Requirements
        Route::post('log', 'LogController@store');

        // Auth
        Route::post('signin', 'AuthController@signin');

        // Business
        Route::group(['middleware' => 'jwt'], function () {
            Route::resource('partners', 'PartnerController');
            Route::get('profile', 'UsersController@profile');
            Route::get('transactions', 'CustomerController@transactions');
            Route::post('requestorder', 'OrderController@requestOrder');
            Route::post('order', 'OrderController@confirmOrder');
            Route::put('order/{order_id}/status', 'OrderController@updateStatus');

            Route::post('profile/avatar', 'AvatarController@customerAvatar');

            Route::group(['middleware' => 'jwt.admin'], function () {
                Route::resource('jobs', 'JobController');
                Route::resource('categories', 'CategoriesController');
                Route::resource('discounts', 'DiscountController');
                Route::post('partner/avatar/{id}', 'AvatarController@partnerAvatar');
            });
        });

        Route::post('register', 'CustomerController@register');
        Route::get('telegram', 'NotificationController@telegram');
    });

    Route::get('/mailable', function () {
        $invoice = App\User::find(1);

        return new App\Mail\VerifyNewRegisteredCustomer($invoice);
    });
});
